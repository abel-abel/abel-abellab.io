---
title: Hello World
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)

### 一些测试

#### 插入图片

插入图片链接，这是我现在的桌面：

![Slackware KDE4 desktop](http://wstaw.org/m/2017/02/14/plasma-desktopL28693.png)

#### 插入程序块

这是一段Racket程序：

``` scheme
;; lookup a variable in an environment
;; Do NOT change this function
(define (envlookup env str)
  (cond [(null? env) (error "unbound variable during evaluation" str)]
        [(equal? (car (car env)) str) (cdr (car env))]
        [#t (envlookup (cdr env) str)]))
```

这是一段Ruby程序：

``` ruby
class Let < GeometryExpression
  # *add* methods to this class -- do *not* change given code and do not
  # override any methods
  # Note: Look at Var to guide how you implement Let
  def initialize(s,e1,e2)
    @s = s
    @e1 = e1
    @e2 = e2
  end

  def eval_prog env
    @e2.eval_prog([[@s, @e1.eval_prog(env)]].concat(env))
  end

  # I didn't add this function before!!
  def preprocess_prog
    Let.new(@s, @e1.preprocess_prog, @e2.preprocess_prog)
  end
end

```

这是一段SML程序：

``` sml
(* Didn't understand the problem's meaning first.
I fix it after I saw the error message of the autograder.
 *)
fun preprocess_prog exp =
  case exp of
      LineSegment(x1, y1, x2, y2) =>
      if real_close(x1, x2) then
	  (if real_close(y1, y2) then Point(x1, y1)
	   else if y1 > y2 then LineSegment(x2, y2, x1, y1)
	   else exp)
      else if x1 > x2 then LineSegment(x2, y2, x1, y1)
      else exp
    | Intersect(e1, e2) => Intersect(preprocess_prog e1,
				    preprocess_prog e2)
    | Let(s, e1, e2) => Let(s, preprocess_prog e1, preprocess_prog e2)
    | Shift(dx, dy, e) => Shift(dx, dy, preprocess_prog e)
    | _ => exp;

```

只是测试一下有没有代码语法高亮。

发现racket没有语法高亮，那就试一试改成scheme看看。另外，如果插入的图片是外部的链接，可能网页会加载的很慢，尤其是大图片。文字和图片链接之间最好有空行，这样生成的网页美观一点。

发现scheme也没有语法高亮。那试一试lisp程序：

``` lisp
(defun next-prime (n)
  "return the first prime number which is greater than n"
  (loop for i from (1+ n) when (primep i)
     return i))

;; NOTE: Don't write as: `(,x ,(gensym)) because of the macro expansion time should be later.
(defmacro with-gensyms ((&rest rest) &body body)
  `(let ,(loop for x in rest
	    collect `(,x (gensym)))
     ,@body))
```

发现lisp程序也没有高亮，可能需要插件吧。目前就这样吧。能用就行。

电脑旧了，刚才竟然过热关机了。果然还是不能同时开太多东西。CPU负担不能太大，所以我打算把部分工作放到树莓派上面来做。比如写不需要使用IDE的程序、看书看文档。一般情况下本地就打开一个浏览器查东西就够了。